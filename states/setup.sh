# Distribution: Jezarch
# ===================


# Setup
CopyFile '/etc/sudoers.d/jezarch'

# Add standard packages
AddPackage sudo # Give certain users the ability to run some commands as root
AddPackage go # Core compiler tools for the Go programming language
AddPackage git # the fast distributed version control system

# Add devel packages
AddPackage linux-headers # Headers and scripts for building modules for the Linux kernel

# Bug, packagegroup does not work well :(
# AddPackageGroup base-devel
AddPackage autoconf
AddPackage automake
AddPackage binutils
AddPackage bison
AddPackage fakeroot
AddPackage file
AddPackage findutils
AddPackage flex
AddPackage gawk
AddPackage gcc
AddPackage gettext
AddPackage grep
AddPackage groff
AddPackage gzip
AddPackage libtool
AddPackage m4
AddPackage make
AddPackage patch
AddPackage pkgconf
AddPackage sed
AddPackage sudo
AddPackage texinfo
AddPackage which


# Add foreign packages
AddPackage --foreign aconfmgr-git # A configuration manager for Arch Linux
AddPackage --foreign yay # Yet another yogurt. Pacman wrapper and AUR helper written in go.
