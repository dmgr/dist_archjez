# Distribution: Jezarch
# ===================

Import base_arch inherit system
Load state setup

# New recipes

# New packages
# -------------------
AddPackage tree # A directory listing program displaying a depth indented list of files
AddPackage ncdu # Disk usage analyzer with an ncurses interface
AddPackage htop # Interactive process viewer
AddPackage iftop # Display bandwidth usage on an interface
AddPackage iotop # View I/O usage of processes



# New configured packages
# -------------------
AddPackage vim # Vi Improved, a highly configurable, improved version of the vi text editor
CopyFile /etc/vimrc

AddPackage openssh # Premier connectivity tool for remote login with the SSH protocol
# Todo: Securise cyphers

# Distro settings
# -----------
CopyFile /etc/motd
CopyFile /etc/profile.d/jezarch.sh

