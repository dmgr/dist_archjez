
# Configure archjez
ARCHJEZ_SUDO_CONF=${ARCHJEZ_SUDO_CONF:-true}
ARCHJEZ_INSTALL_YAY=${ARCHJEZ_INSTALL_YAY:-true}
ARCHJEZ_INSTALL_ACONFMGR=${ARCHJEZ_INSTALL_ACONFMGR:-true}

# Override
SYS_DIST=${SYS_DIST:-JezArch}
SYS_SERVICE_ACCOUNT=${ARCHJEZ_SERVICE_ACCOUNT:-true}

Require base_arch https://framagit.org/dmgr/base_archlinux.git

