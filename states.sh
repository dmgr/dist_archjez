
# Import base distribution
Load lib

# Load distro specifics
Load state setup
Load state dist

# Ignore system user settings
IgnoreStart
Load state user
IgnoreStop

