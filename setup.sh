
# Parent import
Import base_arch lib arch
Import base_arch setup



JezArchInstall ()
{
  if $ARCHJEZ_SUDO_CONF; then

    # Install yay
    if $ARCHJEZ_INSTALL_YAY; then
      ArchInstallYay

      # Install aconfmgr
      if $ARCHJEZ_INSTALL_ACONFMGR; then
        ArchInstallAconfmgr
      fi

    fi
  fi

  if ${ARCHJEZ_INSTALL_BRANDING-}; then
    Log 'Install branding ...\n'
  fi
}


LogEnter 'Configuring jezarch\n'
  ArchAdminAccount $SYS_USER_ACCOUNT
  JezArchInstall
LogLeave 'jezarch has been configured\n'



